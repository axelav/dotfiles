#!/usr/bin/env zsh

# create a new directory and enter it
function mkd {
  mkdir -p "$@" && cd "$@"
}

# cd into whatever is the forefront finder window.
function cdf {
    cd "`osascript -e 'tell app "Finder" to POSIX path of (insertion location as alias)'`"
}

# use git's colored diff when available
hash git &>/dev/null
if [ $? -eq 0 ]; then
  function diff {
    git diff --no-index --color-words "$@"
  }
fi

# extract archives - use: extract <file>
# credits to http://dotfiles.org/~pseup/.bashrc
function extract {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2) tar xjf $1 ;;
      *.tar.gz) tar xzf $1 ;;
      *.bz2) bunzip2 $1 ;;
      *.rar) rar x $1 ;;
      *.gz) gunzip $1 ;;
      *.tar) tar xf $1 ;;
      *.tbz2) tar xjf $1 ;;
      *.tgz) tar xzf $1 ;;
      *.zip) unzip $1 ;;
      *.Z) uncompress $1 ;;
      *.7z) 7z x $1 ;;
      *) echo "'$1' cannot be extracted via extract()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

function backup {
  rsync -avz --delete --progress --exclude-from "$HOME/.rsync_excludes" $HOME /Volumes/BigBoi;
  date >> /Volumes/BigBoi/BACKUP.log;
}

# https://twitter.com/climagic/status/672862397015658496
shrug () {
  echo -n "¯\_(ツ)_/¯" | pbcopy;
  echo "¯\_(ツ)_/¯ copied to your clipboard";
}

x () {
  if hash xplr 2>/dev/null; then
    v $(xplr);
  fi
}

v () {
  if hash nvim 2>/dev/null; then
    if [ $1 ]; then
      nvim $@;
    elif [ -f Session.vim ]; then
      nvim -S;
    else
      nvim;
    fi
  else
    vim $@;
  fi
}

weather () {
  curl http://wttr\.in/$@ | sed -e "s:226m:202m:g" # for light theme
}

function mog () {
  if hash mogrify 2>/dev/null; then
    mogrify -format jpg $@;
  else
    echo "Install imagemagick";
  fi
}

function flac2m4a {
  DIRECTORY=$1

  for filename in "$DIRECTORY"/*.flac
  do
    echo "Processing $filename"
    ffmpeg -i "$filename" -y -v 0 -c:a alac -c:v copy "${filename%.*}.m4a"
    echo "Success! ${filename%.*}.m4a"
    rm "$filename"
  done

  # TODO?
  # find $DIRECTORY -type f -name '*.flac' -not -empty -print0 | parallel -0 -j -1 ffmpeg -loglevel fatal -i {} {.}.m4a
}

function imageoptim {
  DIRECTORY=$1

  for filename in "$DIRECTORY"/*.jpg
  do
    echo "Optimizing $filename"
    magick "$filename" -strip -interlace Plane -sampling-factor 4:2:0 -define jpeg:dct-method=float -quality 85% "${filename%.*}.85.jpg"
    echo "Success! ${filename%.*}.85.jpg"
  done
}
