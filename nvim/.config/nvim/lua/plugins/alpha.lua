-- Disable the "alpha" start page.
return {
  {
    "goolord/alpha-nvim",
    enabled = false,
  },
}
